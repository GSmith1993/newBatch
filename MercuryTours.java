package one;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MercuryTours {

	public static WebDriver dr;

	public static void main(String[] args) {

		MercuryTours mt = new MercuryTours();
		mt.launch();
		mt.login();
//		mt.verifyLogin();
//		mt.findFlight();
//		mt.selectFlight();
//		mt.bookFlight();

//		 mt.logout();
	}

	@Test(priority = 0)
	void launch() {

		File file = new File("C:/Users/lavon/Downloads/selenium-java-2.53.1/selenium-2.53.1/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());

		dr = new ChromeDriver();

		dr.get("http://newtours.demoaut.com");
	}

	@Test(priority = 1)
	void login() {
		dr.findElement(By.name("userName")).sendKeys("ajax");

		dr.findElement(By.name("password")).sendKeys("pool");
		;

		dr.findElement(By.name("login")).click();
		Assert.assertEquals(dr.getTitle(), dr.getTitle());


	}

	@Test(priority = 2)
	void verifyLogin() {
		Assert.assertEquals(dr.getTitle(), dr.getTitle());

		if (dr.getTitle().equals("Find a Flight: Mercury Tours:")) {
			System.out.println("Valid Login");
		} else {
			System.out.println("Not Valid");
		}

	}

	@Test(priority = 3)
	void findFlight() {

		if (dr.getTitle().equals("Find a Flight: Mercury Tours:")) {
			System.out.println("valid");
		} else {
			dr.close();
		}

		Assert.assertTrue(dr.getTitle().equals("Find a Flight: Mercury Tours:"));

		dr.findElement(By.xpath("//input[@value=\'oneway\']")).click();
		dr.findElement(By.xpath("//input[@value=\'First\']")).click();
		dr.findElement(By.xpath("//input[@name=\'findFlights\']")).click();
		;
	}

	@Test(priority = 4)
	void selectFlight() {
		if (dr.getTitle().equals("Select a Flight: Mercury Tours")) {
			System.out.println("valid");
		} else {
			dr.close();
		}
		Assert.assertTrue(dr.getTitle().equals("Select a Flight: Mercury Tours"));

		dr.findElement(By
				.xpath("/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table[1]/tbody/tr[9]/td[1]/input"))
				.click();
		dr.findElement(By
				.xpath("/html/body/div/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[5]/td/form/table[2]/tbody/tr[5]/td[1]/input"))
				.click();
		dr.findElement(By.name("reserveFlights")).click();

	}

	@Test(priority = 5)
	void bookFlight() {

		Assert.assertTrue(dr.getTitle().equals("Book a Flight: Mercury Tours"));

		dr.findElement(By.name("passFirst0")).sendKeys("Jimmy");
		dr.findElement(By.name("passLast0")).sendKeys("Newton");
		dr.findElement(By.name("creditnumber")).sendKeys("123456789");
		dr.findElement(By.name("buyFlights")).click();

	}

	@Test(priority = 6)
	void logout() {

		dr.close();
	}
	
//	void configProp() {
//		
//		Properties prop = new Properties();
//		OutputStream out = null;
//		
//		try{
//			out = new FileOutputStream("MercuryTests.properties");
//			prop.setProperty(key, value);
//		}
//		
//		
//		
//	}

}

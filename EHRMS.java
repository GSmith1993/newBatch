package one;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class EHRMS {
	
	public static WebDriver dr;
	
//	public static void main(String[] args) {
//
//		EHRMS mt = new EHRMS();
//		mt.launch();
//		mt.login();
//		mt.leaveRequest();
//		mt.timeUpdate();
//
//		mt.logout();
//	}

	
	@Test(priority=0)
	void launch() {

		File file = new File("C:/Users/lavon/Downloads/selenium-java-2.53.1/selenium-2.53.1/chromedriver.exe");
		System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());

		dr = new ChromeDriver();

		dr.get("http://192.168.60.11:7001/eHRMS/login.jsp");
	}
	
	@Test(priority=1)
	void login() {
		dr.findElement(By.name("j_username")).clear();

		dr.findElement(By.name("j_username")).sendKeys("guest");

		dr.findElement(By.name("j_password")).sendKeys("off2hand");;

		dr.findElement(By.xpath("/html/body/table/tbody/tr[2]/td/form/table/tbody/tr/td/table/tbody/tr[3]/td/table/tbody/tr[5]/td[2]/input[1]")).click();
		Assert.assertEquals(dr.getTitle(), dr.getTitle());
		
	}
	
	@Test(priority=2)
	void leaveRequest() {
		
		dr.findElement(By.xpath("//*[@id='accordion']/div[2]/table/tbody/tr/td[2]/a")).click();
		dr.findElement(By.xpath("//*[@id='dt_example']/table/tbody/tr[2]/td[2]/table/tbody/tr/td/form/table/tbody/tr[3]/td/p/a/font")).click();
		dr.findElement(By.xpath("//*[@id='fromDate']")).sendKeys("4.26.16");
		dr.findElement(By.xpath("//*[@id='toDate']")).sendKeys("5.26.16");
		dr.findElement(By.xpath("//*[@id='noOfDays']")).sendKeys("34");
		dr.findElement(By.xpath("//*[@id='reason']")).sendKeys("Felt Like It");
		dr.findElement(By.xpath("//*[@id='newemp']/div/table/tbody/tr[8]/td[2]/input")).click();;
//		dr.findElement(By.xpath("//*[@id='newemp']/div/table/tbody/tr[1]/td/a")).click();
	}
	
	@Test(priority=3)
	void timeUpdate() {
		
		dr.findElement(By.xpath("//*[@id='accordion']/div[3]/table/tbody/tr/td[2]/a")).click();
		dr.findElement(By.xpath("//*[@id='dt_example']/table/tbody/tr[2]/td[2]/table/tbody/tr/td/table/tbody/tr[3]/td/p/a/font")).click();
		dr.findElement(By.xpath("//*[@id='date']")).sendKeys("6.25.2016");
		dr.findElement(By.xpath("//*[@id='hours']")).sendKeys("32");
		dr.findElement(By.xpath("//*[@id='jobDone']")).sendKeys("Party");
		dr.findElement(By.xpath("//*[@id='newemp']/div/table/tbody/tr[8]/td[2]/textarea")).sendKeys("Awesomesauce");
//		dr.findElement(By.xpath("//*[@id='newemp']/div/table/tbody/tr[10]/td[2]/input")).click();

		
		
	}
	
	@Test(priority=4)
	void logout() {
		dr.quit();
	}

}

count=1
while [ $count -le 10 ]
do 
echo "Enter a Number: "
read num

if [ 'expr $num % 2' -eq 0 ]
then 
echo "$num" >> even.txt
count='expr $count + 1'
else
echo "$num" >> odd.txt
count='expr $count + 1'
fi
done

echo "Evens in even.txt"
echo "Odds in odd.txt"

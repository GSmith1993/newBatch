package com.revature.webdriver;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

public class Convert2XML {

	public static void main(String[] args) throws JAXBException {
		EmployeeInfo empInfo = new EmployeeInfo();

		empInfo.setAge(25);
		empInfo.setEmployeeId(500);
		empInfo.setEmployeeName("Tom");
		empInfo.setSalary(1000000);

		File file = new File("newlyGenerated.xml");
		JAXBContext jct = JAXBContext.newInstance(EmployeeInfo.class);
		Marshaller jMar = jct.createMarshaller();
		jMar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jMar.marshal(empInfo, file);
		jMar.marshal(empInfo, System.out);
	}
}

package com.revature.webdriver;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class Xml2Config {

	public static void main(String[] args) throws JAXBException {

		try {

			File file = new File("newlyGenerated.xml");

			JAXBContext jct = JAXBContext.newInstance(EmployeeInfo.class);

			Unmarshaller unMar = jct.createUnmarshaller();

			EmployeeInfo emp = (EmployeeInfo) unMar.unmarshal(file);

			System.out.println("Employee: ");
			
			System.out.println(emp.age + " " + emp.employeeId + " " + emp.employeeName + " " + emp.salary);
		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}

}
